var ORDERS_SHEET = "Orders";
var HISTORY_SHEET = "History";
var MAIN_SHEET = "Main Sheet";
var GROUP_SHEET = "ItemGroup DB & Finder"
var ITEM_SHEET = "ItemDB"
var DAY_MILLIS = 86400000;
var HISTORY_WIDTH = 21;
var LOG_TYPES = {
  STATUS: 0,
  WARN: 1,
  ERROR: 2
};
var LOG_CELL = "E3";
var COLOR_CELL = "D4";
var REGION_CELL = "P2"
var ORDER_WIDTH = 12 //1+(2*i)

format = function(format) {
  var args = Array.prototype.slice.call(arguments, 1);
  return format.replace(/{(\d+)}/g, function(match, number) { 
    return typeof args[number] != 'undefined'
    ? args[number]
    : match
    ;
  });
};

function minutesFromIssue(date){
  var now = Date.now()
  var issued = new Date(date)
  return (now-issued)/1000/60
}

function getDayMinus(date, days){
  return new Date(date.getTime() - days*DAY_MILLIS)
}

function getToday(){
  return getDayMinus(getTodayTemp(), 1)
}

function getTodayTemp(){
  var td = new Date()
  return new Date(td.getYear(), td.getMonth(), td.getDate())
}

function getJustDate(date){
  return new Date(date.getYear(), date.getMonth(), date.getDate())
}

function getDateDiff(first, second){
  var diff = (second - first)/DAY_MILLIS
  return(Math.round(diff))
}

function findChildrenDict(dict, target){
  var new_dict = {}
  var done = true
  for(var key in dict){
    if(dict[key] == "None"){
      if(key == target){
        new_dict[key] = "None"
      }
    }else{
      if(dict[key] == target){
        new_dict[key] = target
      }else{
        done = false
        if(dict[dict[key]] == undefined){
          dict[dict[key]]="None"
        }
        new_dict[key] = dict[dict[key]]
      }
    }
  }
  if(done){
    return new_dict
  }else{
    return findChildrenDict(new_dict, target)
  }
}

function findChildren(target_group, table){
  var dict  = {};

  for each(var i in table){ //Not an error in Google Apps Script
    dict[i[0]] = i[1]
  }
  delete dict[""]
  var final_dict = findChildrenDict(dict, target_group)
  var final = [];
  for(var key in final_dict){
    final.push(key)
  }
  final = makeCollumn(final)
  return final
}

function FIND_CHILDREN(TARGET_GROUP, TABLE){
    return findChildren(TARGET_GROUP, TABLE);
}

function clearLog(){
  SpreadsheetApp.getActiveSpreadsheet().getSheetByName(MAIN_SHEET).getRange(LOG_CELL).clearContent()
  SpreadsheetApp.getActiveSpreadsheet().getSheetByName(MAIN_SHEET).getRange(COLOR_CELL).setBackground("white")
  SpreadsheetApp.flush()
}

function addLog(message, type){
  var curr = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(MAIN_SHEET).getRange(LOG_CELL).getValue();
  var bg = "#a4c2f4" //light-blue
  if(type == LOG_TYPES.ERROR){
    message = "ERROR-> " + message;
    bg = "#e06666" //light red 1
  }
  SpreadsheetApp.getActiveSpreadsheet().getSheetByName(MAIN_SHEET).getRange(LOG_CELL).setValue(curr + message + "\n");
  SpreadsheetApp.getActiveSpreadsheet().getSheetByName(MAIN_SHEET).getRange(COLOR_CELL).setBackground(bg)
  SpreadsheetApp.flush()
}

function getStation(name){
  var url = "https://esi.evetech.net/latest/search/";
  var resp = UrlFetchApp.fetch(url + "?categories=station&strict=true&search=" + name)
  var json = JSON.parse(resp.getContentText())
  if("station" in json){
    return json.station
  }else{
    addLog("No station with that name found", LOG_TYPES.ERROR)
    return -1
  }
}

function getRegionFromStation(station_id){
  var station_url = "https://esi.evetech.net/latest/universe/stations/";
  var station_resp = UrlFetchApp.fetch(station_url + station_id + "/");
  var system = JSON.parse(station_resp.getContentText()).system_id
  var system_url = "https://esi.evetech.net/latest/universe/systems/"
  var system_resp = UrlFetchApp.fetch(system_url + system + "/");
  var constl = JSON.parse(system_resp.getContentText()).constellation_id
  var constl_url = "https://esi.evetech.net/latest/universe/constellations/"
  var constl_resp = UrlFetchApp.fetch(constl_url + constl + "/");
  return JSON.parse(constl_resp.getContentText()).region_id
}

function getRegionName(region_id){
  var url = "https://esi.evetech.net/latest/universe/regions/";
  var resp = UrlFetchApp.fetch(url + region_id + "/")
  return JSON.parse(resp.getContentText()).name
}

function getItemName(item){
  var url = "https://esi.evetech.net/latest/universe/types/"
  var resp = UrlFetchApp.fetch(url + item)
  return JSON.parse(resp.getContentText()).name
}

function getItemHistory(region, item, period){
  var url = "https://esi.tech.ccp.is/latest/markets/"
  
  var history = []
  
  var today = getToday()
  var resp = UrlFetchApp.fetch(url + region + "/history/?type_id=" + item)
  var json = JSON.parse(resp.getContentText())
  //Process the orders
  // for(var i=json.length-1; i>=json.length-period; i--){
  var k = 0 //k indicates the absolute position (even including days missed) TODO: Add better explanation
  for(var i=0; i < period;){
    var his = json[json.length - i - 1]
//    if(his == undefined){
//      history.push({date: "ERROR FOR ID: ", volume: 0, avg: 0, max: 0, min: 0})
//      continue
//    }
    var date = getJustDate(new Date(his["date"]))
    //If there is a gap mark zero volume, same avg price
    var days_missed = (today - date) / DAY_MILLIS - k
    if(days_missed > 0){
      for(var j=0; j<days_missed; j++){
        var jdate = getDayMinus(date, -(days_missed-j))
        if((today-jdate)/DAY_MILLIS < period){
          var avg = his["average"]
          history.push({date: jdate, volume: 0, avg: avg, max: avg, min: avg})
        }else{
          break
        }

      }
      k = (today - date)/DAY_MILLIS //update k
    }
    
    //If period is overshot break
    if((today - date)/DAY_MILLIS >= (period)){
      break;
    }
    
    history.push({date: date, volume: his["volume"], avg: his["average"], max: his["highest"], min: his["lowest"]})
    k++
    i++
  }
  return history
}

function craftFormula(){
  var desired_col = "A"
  var exp_arr = []
  arr = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(GROUP_SHEET).getRange("H2:H").getValues()
  for (var i=0; i<arr.length; i++){
    if(arr[i][0]==""){
      break;
    }
    exp_arr[i]=format("({2}!{0}2:{0}={1})", "L", arr[i][0], ITEM_SHEET)
  }
  var final = format("=FILTER({2}!{0}2:{0},{1})",desired_col,exp_arr.join('+'), ITEM_SHEET)
  SpreadsheetApp.getActiveSpreadsheet().getSheetByName(GROUP_SHEET).getRange("I2").setFormula(final)
}

function makeCollumn(array){
  var new_arr = []
  for each(var i in array){
    new_arr.push([i])
  }
  return new_arr
}

function makeTable(obj, col){
  var table = []
  for(var i=0; i<obj.length; i++){
    var subtable = []
    for(var k=0; k<col.length; k++){
      var colname = col[k]
      subtable.push(obj[i][colname])
    }
    table.push(subtable)
  }
  return table
}

function makeOrderTable(name, obj, col){
  var table = []
  var length = Math.max(obj.sell.length, obj.buy.length)
  for(var i=0; i<length; i++){
    var subtable = []
    if(i == 0){
      subtable[0] = name
    }else{
      subtable[0] = ""
    }
    for(var k=1; k<2*col.length+1; k++){
      if(k<col.length+1){
        var colname = col[k-1]
        subtable.push((obj.sell[i] != null) ? obj.sell[i][colname] : "")
      }else{
        var colname = col[k-(col.length+1)]
        subtable.push((obj.buy[i] != null) ? obj.buy[i][colname] : "")
      }
    }
    table.push(subtable)
  }
  return table
}

function makeHistoryTable(name, obj, col){
  var table = []
  for(var i=0; i<obj.length; i++){
    var subtable = []
    if(i == obj.length-1){
      subtable[0] = name
      // subtable[0] = format("=VLOOKUP({0},ItemDB!A2:C,3)", id)
      // format("GETORDERS({0},{1},{2})",region, item, max_orders)
    }else{
      subtable[0] = ""
    }
    for(var k=0; k<col.length; k++){
      var colname = col[k]
      subtable.push(obj[i][colname])
    }
    table.push(subtable)
  }
  return table
}

function GETORDERS(region, item, max_orders){
  var orders = getItemOrders(region, item, max_orders)
  var cols = ["price", "volume", "issued"]
  var name = getItemName(item)
  var order_table = makeOrderTable(name, orders, cols)
  return order_table
}



function GETHISTORY(region, item, period){
  var history = getItemHistory(region, item, period)
  var cols = ["date", "avg", "min", "max", "volume"]
//  var history_table = makeTable(history, cols)
  var name = getItemName(item)
  var history_table = makeHistoryTable(name, history, cols)
  return history_table.reverse()
}

//TODO Rename initCol to Row
function generateOrderFormula(init_row, region, item, max_orders){
  var arr = []
  for(var i=0; i<max_orders; i++){
    var temp_arr = []
    for(var k=0; k<ORDER_WIDTH; k++){
      temp_arr.push("")
    }
    arr.push(temp_arr)
  }
  arr[0][0] = format("GETORDERS({0},{1},{2})",region, item, max_orders)
  arr[0][7] = format("=MIN(MIN(D{0}:D{1}),MIN(G{0}:G{1}))",init_row, init_row+max_orders-1)
  arr[0][8] = format("=MIN(1, H{0}/'Main Sheet'!E15)",init_row)
  arr[0][9] = format("=I{0}*(VLOOKUP(A{0},History!A2:T,20,FALSE))",init_row) //WARINING Can change very easily
  arr[0][10] = format("=VLOOKUP(A{0},History!A2:T,7,FALSE)", init_row)
  arr[0][11]= format("I{0}*J{0}*VLOOKUP(A{0},History!A2:U,21,FALSE)", init_row)
  return arr
}

function generateHistoryFormula(initCol, region, item, period){
  //Make empty array
  var arr = []
  for(var i=0; i<period; i++){
    var temp_arr = []
    for(var k=0; k<HISTORY_WIDTH; k++){
      temp_arr.push("")
    }
    arr.push(temp_arr)
  }
  arr[0][0] = format("GETHISTORY({0},{1},{2})",region, item, period)
  arr[0][6] = format("FLOOR(SUM(F{0}:F{1})/{2})",initCol, initCol+period-1, period)
  arr[0][8] = format("SUMPRODUCT(C{0}:C{1},F{0}:F{1})/SUM(F{0}:F{1})",initCol, initCol+period-1)
  arr[0][13] = format("SUM(J{0}:J{1})/SUM(K{0}:K{1})",initCol, initCol+period-1)
  arr[0][14] = format("SUM(L{0}:L{1})/SUM(M{0}:M{1})",initCol, initCol+period-1)
  arr[0][15] = format("LINEST(C{0}:C{1},B{0}:B{1})", initCol, initCol+period-1)
  arr[0][17] = format("P{0}/ (MAX(C{0}:C{1})-MIN(C{0}:C{1}))", initCol, initCol+period-1)
  arr[0][17] = format("IF(P{0}<>0, P{0}/ (MAX(C{0}:C{1})-MIN(C{0}:C{1})), 0)", initCol, initCol+period-1)
  arr[0][18] = format("((O{0}-N{0})*MIN(SUM(K{0}:K{1}),SUM(M{0}:M{1})))/10^6", initCol, initCol+period-1)
  arr[0][19] = format("S{0}/{1}", initCol, period)

  //Formula: https://www.desmos.com/calculator/y8odsge2de
  arr[0][20] = format("IFS(R{0}<-0.1, EXP(4*(R{0}+(1/4)*LN(3/4)+0.1)), R{0}<=0.3, -5*(R{0}-0.2)^2+1.2, R{0}>0.3, EXP(-(1/1.15)*(R{0}-0.3-(LN(1/1.15)/-1.15))))", initCol)
  for(var i=0; i<period; i++){
    arr[i][7] = format("IF(D{0}<>E{0},IF(E{0}-C{0}>C{0}-D{0}, (C{0}-D{0})/(E{0}-D{0}), 1-((E{0}-C{0})/(E{0}-D{0}))), FALSE)",initCol+i)
    arr[i][9] = format("IFS(E{1}<I{0}, C{1}*F{1}, D{1}<I{0}, D{1}*(1-H{1})*F{1},TRUE,0)",initCol, initCol+i)
    arr[i][10] = format("IFS(E{1}<I{0}, F{1}, D{1}<I{0}, (1-H{1})*F{1},TRUE,0)",initCol, initCol+i)
    arr[i][11] = format("IFS(D{1}>I{0}, C{1}*F{1}, E{1}>I{0}, E{1}*H{1}*F{1},TRUE,0)",initCol, initCol+i)
    arr[i][12] = format("IFS(D{1}>I{0}, F{1}, E{1}>I{0}, H{1}*F{1},TRUE,0)",initCol, initCol+i)
  }
  return arr
}

function generateMultipleHistory(region, items, period){
  SpreadsheetApp.getActiveSpreadsheet().getSheetByName(HISTORY_SHEET).getRange("A2:U").clearContent();
  var len = items.length
  for(var i=0; i<len; i++){
    if(i%50==0){
      addLog("Current: "+ i, 0)
    }
    var row = 2+i*period
    var f = generateHistoryFormula(row, region, items[i], period)
    SpreadsheetApp.getActiveSpreadsheet().getSheetByName(HISTORY_SHEET).getRange(row,1,period,HISTORY_WIDTH).setFormulas(f)
  }
}

function generateMultipleOrders(region, items, max_orders){
  SpreadsheetApp.getActiveSpreadsheet().getSheetByName(ORDERS_SHEET).getRange("A3:K").clearContent();
  var len = items.length
  for(var i=0; i<len; i++){
    var row = 3+i*max_orders
    var f = generateOrderFormula(row, region, items[i], max_orders)
    SpreadsheetApp.getActiveSpreadsheet().getSheetByName(ORDERS_SHEET).getRange(row,1,max_orders,ORDER_WIDTH).setFormulas(f)
  }
}

function trimOrder(json){
  return {id: json.type_id, price: json.price, volume: json.volume_remain, duration: json.duration, issued: minutesFromIssue(json.issued)}
}

function orderCompare(a, b){
  return a.price - b.price
}

function getItemOrders(region, item, max_orders){
  var buy_arr = [], sell_arr = []
  var url = "https://esi.tech.ccp.is/latest/markets/"
  
  var history = []
  
  var today = getToday()
  var resp = UrlFetchApp.fetch(url + region + "/orders/?type_id=" + item)
  var json = JSON.parse(resp.getContentText())
  for(var i=0; i<json.length; i++){
    if(json[i].is_buy_order == true){
      buy_arr.push(trimOrder(json[i]))
    }else{
      sell_arr.push(trimOrder(json[i]))
    }
  }
  
  sell_arr.sort(orderCompare);
  sell_arr = sell_arr.splice(0, max_orders)
  buy_arr.sort(orderCompare);
  buy_arr.reverse()
  buy_arr = buy_arr.splice(0, max_orders);

  return {sell:sell_arr, buy: buy_arr}
}

// generateOrderFormula(initCol, region, item, max_orders)

function getDesiredItems(arr){
  var new_arr = []
  for(var i=0; i<arr.length; i++){
    if(arr[i][0] != ""){
      new_arr.push(arr[i][0])
    }
  }
  return new_arr
}

function onGroupFormula(){
  if(SpreadsheetApp.getActiveSpreadsheet().getSheetByName(MAIN_SHEET).getRange("E2").getValue() != true){
    //  return
  }
  
  SpreadsheetApp.getActiveSpreadsheet().getSheetByName(MAIN_SHEET).getRange("E2").setValue(false) //Return button to false

  clearLog()
  addLog("Crafting groups formula...", LOG_TYPES.STATUS)
  craftFormula()
  addLog("Done", LOG_TYPES.STATUS)
}

function onGO(){
  if(SpreadsheetApp.getActiveSpreadsheet().getSheetByName(MAIN_SHEET).getRange("E1").getValue() != true){
  //  return
  }

  SpreadsheetApp.getActiveSpreadsheet().getSheetByName(MAIN_SHEET).getRange("E1").setValue(false) //Return button to false
  
  var period = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(MAIN_SHEET).getRange("B2").getValue()

  var station = getStation(SpreadsheetApp.getActiveSpreadsheet().getSheetByName(MAIN_SHEET).getRange("B3").getValue())
  if (station != -1){
    addLog("Found station, Identifying region...", LOG_TYPES.STATUS)
  }

  var region_id = getRegionFromStation(station);
  SpreadsheetApp.getActiveSpreadsheet().getSheetByName(ORDERS_SHEET).getRange(REGION_CELL).setValue(region_id)
  var name = getRegionName(region_id);
  SpreadsheetApp.getActiveSpreadsheet().getSheetByName(MAIN_SHEET).getRange("B4").setValue(name)

  var items = getDesiredItems(SpreadsheetApp.getActiveSpreadsheet().getSheetByName(GROUP_SHEET).getRange("I2:I").getValues())
  Logger.log(items)
  addLog("Crafting history formula...", LOG_TYPES.STATUS)
  generateMultipleHistory(region_id, items, period)
  addLog("Done", LOG_TYPES.STATUS)
  addLog("Crafting orders formula...", LOG_TYPES.STATUS)
  generateMultipleOrders(region_id, items, 16)
  addLog("Done", LOG_TYPES.STATUS)
  // addLog("Crafting history formula...", LOG_TYPES.STATUS)
  // addLog("Done", LOG_TYPES.STATUS)

  //Domain id: 10000043
  //Slasher id: 585
}